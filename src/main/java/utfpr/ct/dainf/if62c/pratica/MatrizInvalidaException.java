/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Joao
 */
public class MatrizInvalidaException extends Exception {
    int numLinhas, numColunas;
    public MatrizInvalidaException(int numLinhas, int numColunas) {
        super(String.format("Matriz de %dx%d não pode ser criada",numLinhas, numColunas));
        this.numLinhas = numLinhas;
        this.numColunas = numColunas;
    }

    public int getNumLinhas() {
        return numLinhas;
    }

    public int getNumColunas() {
        return numColunas;
    }
    
    
}
