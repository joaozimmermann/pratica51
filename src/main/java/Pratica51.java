import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.MatrizesIncompativeisException;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51 {

    public static void main(String[] args) throws MatrizInvalidaException, MatrizesIncompativeisException {
        try{
            Matriz invalida = new Matriz(-3,3);   
        }catch(MatrizInvalidaException bb){
            System.out.println(bb.getLocalizedMessage());
        }
        try{
            Matriz orig = new Matriz(3, 2);
            double[][] m = orig.getMatriz();
            m[0][0] = 0.0;
            m[0][1] = 0.1;
            m[1][0] = 1.0;
            m[1][1] = 1.1;
            m[2][0] = 2.0;
            m[2][1] = 2.1;
            Matriz matriz2 = new Matriz(3,3);
            double[][] n = matriz2.getMatriz();
            n[0][0] = 0.0;
            n[0][1] = 0.1;
            n[0][2] = 0.2;
            n[1][0] = 1.0;
            n[1][1] = 1.1;
            n[1][2] = 1.2;
            n[2][0] = 2.0;
            n[2][1] = 2.1;
            n[2][2] = 2.2;
            Matriz matriz3 = new Matriz(2,3);
            double[][] o = matriz3.getMatriz();
            o[0][0] = 1.0;
            o[0][1] = 0.1;
            o[0][2] = 0.2;
            o[1][0] = 1.0;
            o[1][1] = 1.1;
            o[1][2] = 1.2;
            try {
                Matriz somada = orig.soma(matriz2);
                System.out.println("Matriz original: " + matriz2);
                System.out.println("Matriz somada: " + somada);
            }catch(MatrizesIncompativeisException aa){
                System.out.println(aa.getLocalizedMessage());
            }        
            try {
                Matriz multiplicada; 
                multiplicada = orig.prod(matriz2);
                System.out.println("Matriz original: " + matriz3);
                System.out.println("Matriz multiplicada: " + multiplicada);
            }catch(MatrizesIncompativeisException aa){
                System.out.println(aa.getLocalizedMessage());
            }  
            try {
                Matriz multiplicada; 
                multiplicada = orig.prod(matriz3);
                System.out.println("Matriz original: " + matriz3);
                System.out.println("Matriz multiplicada: " + multiplicada);
            }catch(MatrizesIncompativeisException aa){
                System.out.println(aa.getLocalizedMessage());
            }  
            try {
                Matriz somada = orig.soma(orig);
                System.out.println("Matriz original: " + matriz2);
                System.out.println("Matriz somada: " + somada);
            }catch(MatrizesIncompativeisException aa){
                System.out.println(aa.getLocalizedMessage());
            }
        }catch(MatrizInvalidaException bb){
            System.out.println(bb.getLocalizedMessage());
        }
    }
}